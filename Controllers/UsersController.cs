﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class UsersController : Controller
    {
        private ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public UsersController()
        {
            _context = new ApplicationDbContext();
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));
        }

        // GET: Users/Index
        public ActionResult Index()
        {
            var users = _context.Users.Include(u => u.Roles).ToList();
            var roles = _context.Roles.ToList();

            var userViewModelList = new List<UserViewModel>();

            foreach (var user in users)
            {
                var userViewModel = new UserViewModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    Phone = user.Phone,
                    DrivingLicense = user.DrivingLicense
                };

                userViewModel.Roles = roles
                    .Where(r => user.Roles
                            .Select(u => u.RoleId)
                            .ToArray()
                            .Contains(r.Id))
                    .Select(r => r.Name).ToList();

                userViewModelList.Add(userViewModel);
            }

            return View(userViewModelList);
        }

        //GET: User/Edit/{Id}
        public ActionResult Edit(int id)
        {
            return View(id);
        }

        //GET: User/Edit/{Id:string}
        [Route("users/edit/{stringId}")]
        public ActionResult Edit(string stringId)
        {
            var user = _context.Users.Include(u => u.Roles).Single(u => u.Id == stringId);
            var roles = _context.Roles.ToList();

            var userFormViewModel = new UserFormViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                Phone = user.Phone,
                DrivingLicense = user.DrivingLicense
            };

            userFormViewModel.Roles = roles;
            userFormViewModel.RoleIds = roles
                    .Where(r => user.Roles
                            .Select(u => u.RoleId)
                            .ToArray()
                            .Contains(r.Id))
                    .Select(r => r.Id).ToList();

            return View("Edit", userFormViewModel);
        }

        public ActionResult Save(UserFormViewModel userFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View("Edit", userFormViewModel);
            }

            var roles = _context.Roles.ToList();
            var user = _context.Users.Include(u => u.Roles).Single(u => u.Id == userFormViewModel.Id);

            user.Email = userFormViewModel.Email;
            user.DrivingLicense = userFormViewModel.DrivingLicense;
            user.Phone = userFormViewModel.Phone;

            // Remove all user roles when RoleIds is null
            if (userFormViewModel.RoleIds == null)
            {
                foreach (var role in user.Roles.ToList())
                {
                    _userManager.RemoveFromRole(user.Id, roles.Single(r => role.RoleId == r.Id).Name);
                }
            }
            else {
                foreach (var role in roles)
                {
                    // Add Role
                    if (userFormViewModel.RoleIds.Contains(role.Id) && !user.Roles.Select(r => r.RoleId).ToArray().Contains(role.Id))
                    {
                        _userManager.AddToRole(user.Id, role.Name);
                    }

                    // Remove Roles
                    if (!userFormViewModel.RoleIds.Contains(role.Id) && user.Roles.Select(r => r.RoleId).ToArray().Contains(role.Id))
                    {
                        _userManager.RemoveFromRole(user.Id, role.Name);
                    }
                }
            }
            
            _context.SaveChanges();
            return Redirect("Index");
        }
    }
}