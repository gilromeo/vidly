﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class MoviesController : Controller
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // Movie/Index
        public ViewResult Index()
        {
            if (User.IsInRole(RoleName.CanManageMovies))
            {
                return View("List");
            }
            
            return View("ReadOnlyList");
        }

        // Movie/Details
        public ActionResult Details(int id = 0)
        {
            var movie = _context.Movies
                .Include(m => m.Genre)
                .Where(m => m.Id == id)
                .SingleOrDefault();

            return View(movie);
        }

        // Movie/New
        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult New()
        {
            var movieTypes = _context.MovieTypes.ToList();
            var genres = _context.Genres.ToList();
            var viewModel = new MovieFormViewModel()
            {
                Genres = genres,
                MovieTypes = movieTypes
            };

            return View("MovieForm", viewModel);
        }


        // Movie/Edit/{id}
        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Edit(int id)
        {
            var movie = _context.Movies.SingleOrDefault(c => c.Id == id);
            var viewModel = new MovieFormViewModel(movie)
            {
                Genres = _context.Genres.ToList(),
                MovieTypes = _context.MovieTypes.ToList()
            };

            return View("MovieForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.CanManageMovies)]
        public ActionResult Save(Movie movie)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new MovieFormViewModel(movie)
                {
                    Genres = _context.Genres.ToList(),
                    MovieTypes = _context.MovieTypes.ToList()
                };

                return View("MovieForm", viewModel);
            }

            if (movie.Id == 0)
            {
                // Set DateAdded to today
                movie.DateAdded = DateTime.Now;

                // Set NumberAvailble equal to number in stock
                movie.NumberAvailable = movie.InStock;

                // Save to database
                _context.Movies.Add(movie);
            }
            else
            {
                var movieInDb = _context.Movies.Single(c => c.Id == movie.Id);

                movieInDb.Name = movie.Name;
                movieInDb.ReleasedDate = movie.ReleasedDate;
                movieInDb.GenreId = movie.GenreId;
                movieInDb.NumberAvailable = movie.InStock;
                movieInDb.InStock = movie.InStock;
                movieInDb.MovieTypeId = movie.MovieTypeId;
            }

            _context.SaveChanges();

            return RedirectToAction("Index", "Movies");
        }
    }
}