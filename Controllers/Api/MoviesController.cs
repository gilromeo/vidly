﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using Vidly.Dtos;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers.Api
{
    public class MoviesController : ApiController
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        //GET: /api/movies
        public IHttpActionResult GetMovies(string query = null)
        {
            List<MovieDto> movieDtos = new List<MovieDto>();
            MovieDto movieDto;

            var moviesQuery = _context.Movies
                .Include(m => m.Genre)
                .Include(m => m.Type)
                .Where(m => m.NumberAvailable > 0);

            if (!String.IsNullOrWhiteSpace(query))
            {
                moviesQuery = moviesQuery.Where(m => m.Name.Contains(query));
            }

            var movies = moviesQuery.ToList();

            // Map Movie to MovieDto
            foreach (var movie in movies)
            {
                movieDto = new MovieDto
                {
                    Name = movie.Name,
                    Id = movie.Id,
                    GenreId = movie.GenreId,
                    DateAdded = movie.DateAdded,
                    InStock = movie.InStock,
                    ReleasedDate = movie.ReleasedDate,
                    Genre = new GenreDto()
                    {
                        Id = movie.Genre.Id,
                        Name = movie.Genre.Name
                    },
                    MovieTypeId = movie.Type.Id,
                    MovieType = new MovieTypeDto
                    {
                        Rate = movie.Type.Rate
                    }
                };

                movieDtos.Add(movieDto);
            }

            return Ok(movieDtos);
        }

        //GET: /api/movies/{id}
        public IHttpActionResult GetMovie(int id)
        {
            var movie = _context.Movies.SingleOrDefault(m => m.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            // Map Movie to MovieDto
            MovieDto movieDto = new MovieDto
            {
                Name = movie.Name,
                Id = movie.Id,
                GenreId = movie.GenreId,
                DateAdded = movie.DateAdded,
                InStock = movie.InStock,
                ReleasedDate = movie.ReleasedDate
            };

            return Ok(movieDto);
        }

        //POST: api/movies
        [HttpPost]
        [Authorize(Roles = RoleName.CanManageMovies)]
        public IHttpActionResult CreateMovie(MovieDto movieDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            // Map MovieDto to Movie
            var movie = new Movie
            {
                Name = movieDto.Name,
                GenreId = movieDto.GenreId,
                DateAdded = movieDto.DateAdded,
                InStock = movieDto.InStock,
                ReleasedDate = movieDto.ReleasedDate
            };

            _context.Movies.Add(movie);
            _context.SaveChanges();

            movieDto.Id = movie.Id;

            return Created(new Uri(Request.RequestUri + "/" + movie.Id), movie);
        }

        //POST: /api/movies/get-datatable-movies
        [HttpPost]
        [Route("api/movies/get-datatable-movies")]
        public IHttpActionResult GetDataTableMovies(DataTableAjaxPostModel tableViewModel)
        {
            _context.Configuration.AutoDetectChangesEnabled = false;

            int filteredResultsCount;
            int totalResultsCount;
            var res = GetDataTableData(tableViewModel, out filteredResultsCount, out totalResultsCount);

            return Json(
                new
                {
                    draw = tableViewModel.draw,
                    recordsTotal = totalResultsCount,
                    recordsFiltered = filteredResultsCount,
                    data = res
                });
        }

        //PUT: api/movies/{id}
        [HttpPut]
        [Authorize(Roles = RoleName.CanManageMovies)]
        public IHttpActionResult UpdateMovie(int id, MovieDto movieDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var movieInDb = _context.Movies.SingleOrDefault(m => m.Id == id);

            if (movieInDb == null)
            {
                return NotFound();
            }

            movieInDb.Name = movieDto.Name;
            movieInDb.GenreId = movieDto.GenreId;
            movieInDb.DateAdded = movieDto.DateAdded;
            movieInDb.InStock = movieDto.InStock;
            movieInDb.ReleasedDate = movieDto.ReleasedDate;

            _context.SaveChanges();

            return Ok();
        }

        //DELETE: api/movies/{id}
        [HttpDelete]
        [Authorize(Roles = RoleName.CanManageMovies)]
        public IHttpActionResult DeleteMovie(int id)
        {
            var movieInDb = _context.Movies.SingleOrDefault(c => c.Id == id);

            if (movieInDb == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(movieInDb);
            _context.SaveChanges();

            return Ok();
        }

        // Private Functions
        private IList<MovieDto> GetDataTableData(DataTableAjaxPostModel tableViewModel, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (tableViewModel.search != null) ? tableViewModel.search.value : null;
            var take = tableViewModel.length;
            var skip = tableViewModel.start;

            string sortBy = "Id";
            bool sortAsc = true;

            if (tableViewModel.order != null)
            {
                //sort on the 1st column
                sortBy = tableViewModel.columns[tableViewModel.order[0].column].name.ToLower();
                sortAsc = tableViewModel.order[0].dir.ToLower() == "asc";
            }

            // search the database taking into consideration table sorting and paging
            var result = GetDataFromDatabase(searchBy, take, skip, sortBy, sortAsc, out filteredResultsCount, out totalResultsCount);
            if (result == null)
            {
                // empty collection...
                return new List<MovieDto>();
            }
            return result;
        }

        private List<MovieDto> GetDataFromDatabase(string searchBy, int take, int skip, string sortBy, bool sortAsc, out int filteredResultsCount, out int totalResultsCount)
        {
            // Create an uninitialized list of Movies
            List<MovieDto> result;

            // Apply pagination, sorting, and filtering for DataTable's data 
            var filterQueryResult = ApplyDatableFilters(searchBy, sortBy, sortAsc, skip, take, out filteredResultsCount);

            result = _context.Movies
                .AsNoTracking()
                .Include(m => m.Genre)
                .Where(m => filterQueryResult
                .Contains(m.Id))
                .Select(movie => new MovieDto
                {
                    Name = movie.Name,
                    Id = movie.Id,
                    GenreId = movie.GenreId,
                    Genre = new GenreDto
                    {
                        Id = movie.Genre.Id,
                        Name = movie.Genre.Name
                    }
                })
                .ToList();

            totalResultsCount = _context.Movies.Count();

            return result;
        }

        private IQueryable<int> ApplyDatableFilters(string searchBy, string sortBy, bool sortAsc, int skip, int take, out int filteredResultsCount)
        {
            var query = _context.Movies.Include(mov => mov.Genre).AsNoTracking().Select(mov => new { Id = mov.Id, Name = mov.Name, Genre = new { Name = mov.Genre.Name } });
            IQueryable<int> result;

            // If DataTable search value is not empty
            if (!String.IsNullOrWhiteSpace(searchBy))
            {
                // filter movies based on the search value
                query = query.Where(mov => mov.Name.Contains(searchBy));
            }

            // get the count of all the filtered movies
            filteredResultsCount = query.Count();

            // Apply sorting
            switch (sortBy)
            {
                // Sort by Movie Name
                case "name":
                    query = sortAsc ? query.OrderBy(mov => mov.Name) : query.OrderByDescending(mov => mov.Name);
                    break;
                // Sort by Genre Name
                case "genre.name":
                    query = sortAsc ? query.OrderBy(mov => mov.Genre.Name) : query.OrderByDescending(y => y.Genre.Name);
                    break;
                // Sort by Movie Id
                default:
                    query = sortAsc ? query.OrderBy(mov => mov.Id) : query.OrderByDescending(y => y.Id);
                    break;
            }
            result = query.Select(y => y.Id).Skip(skip).Take(take);

            return result;
        }
    }
}
