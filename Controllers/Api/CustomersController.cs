﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;
using System.Data.Entity;
using Vidly.ViewModels;

namespace Vidly.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }

        //GET: /api/customers
        public IHttpActionResult GetCustomers(string query = null)
        {
            List<CustomerDto> customerDtos = new List<CustomerDto>();
            CustomerDto customerDto;

            var customersQuery = _context.Customers
                .Include(c => c.MembershipType);

            if (!String.IsNullOrWhiteSpace(query))
            {
                customersQuery = customersQuery.Where(c => c.Name.Contains(query));
            }

            var customers = customersQuery.ToList();

            // Map Customer to CustomerDto
            foreach (var customer in customers)
            {
                customerDto = new CustomerDto
                {
                    Name = customer.Name,
                    Id = customer.Id,
                    IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter,
                    MembershipTypeId = customer.MembershipTypeId,
                    Discount = customer.MembershipType.DiscountRate,
                    Birthday = customer.Birthday,
                    NumberOfAllowableRental = customer.NumberOfAllowableRental,
                    NumberOfCurrentRental = customer.NumberOfCurrentRental,
                    IsDelinquent = customer.IsDelinquent,
                    MembershipType = new MembershipTypeDto
                    {
                        Id = customer.MembershipType.Id,
                        Name = customer.MembershipType.Name
                    }
                };

                customerDtos.Add(customerDto);
            }

            return Ok(customerDtos);
        }

        //GET: /api/customers/{id}
        public IHttpActionResult GetCustomer(int id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customer == null)
            {
                return NotFound();
            }

            // Map Customer to CustomerDto
            CustomerDto customerDto = new CustomerDto
            {
                Name = customer.Name,
                Id = customer.Id,
                IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter,
                MembershipTypeId = customer.MembershipTypeId,
                Birthday = customer.Birthday,
                NumberOfAllowableRental = customer.NumberOfAllowableRental,
                NumberOfCurrentRental = customer.NumberOfCurrentRental,
                IsDelinquent = customer.IsDelinquent
            };

            return Ok(customerDto);
        }

        //POST: api/customers
        [HttpPost]
        public IHttpActionResult CreateCustomer(CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            // Map CustomerDto to Customer
            var customer = new Customer
            {
                Name = customerDto.Name,
                Birthday = customerDto.Birthday,
                IsSubscribedToNewsletter = customerDto.IsSubscribedToNewsletter,
                MembershipTypeId = customerDto.MembershipTypeId,
                NumberOfCurrentRental = customerDto.NumberOfCurrentRental
            };

            _context.Customers.Add(customer);
            _context.SaveChanges();

            customerDto.Id = customer.Id;

            return Created(new Uri(Request.RequestUri + "/" + customer.Id), customer);
        }

        
        //POST: /api/customers/get-datatable-customers
        [HttpPost]
        [Route("api/customers/get-datatable-customers")]
        public IHttpActionResult GetDataTableCustomers(DataTableAjaxPostModel tableViewModel)
        {

            string body = Request.Content.ReadAsStringAsync().Result;
            _context.Configuration.AutoDetectChangesEnabled = false;

            int filteredResultsCount;
            int totalResultsCount;
            var res = GetDataTableData(tableViewModel, out filteredResultsCount, out totalResultsCount);

            return Json(
                new
                {
                    draw = tableViewModel.draw,
                    recordsTotal = totalResultsCount,
                    recordsFiltered = filteredResultsCount,
                    data = res
                });
        }

        //PUT: api/customers/{id}
        [HttpPut]
        public IHttpActionResult UpdateCustomer(int id, CustomerDto customerDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var customerDb = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customerDb == null)
            {
                return NotFound();
            }

            customerDb.Name = customerDto.Name;
            customerDb.Birthday = customerDto.Birthday;
            customerDb.MembershipTypeId = customerDto.MembershipTypeId;
            customerDb.IsSubscribedToNewsletter = customerDto.IsSubscribedToNewsletter;
            customerDb.NumberOfCurrentRental = customerDto.NumberOfCurrentRental;

            _context.SaveChanges();

            return Ok();
        }

        //DELETE: api/customer/{id}
        [HttpDelete]
        public IHttpActionResult DeleteCustomer(int id)
        {
            var customerDb = _context.Customers.SingleOrDefault(c => c.Id == id);

            if (customerDb == null)
            {
                return NotFound();
            }

            _context.Customers.Remove(customerDb);
            _context.SaveChanges();

            return Ok();
        }


        // Private Functions
        private IList<CustomerDto> GetDataTableData(DataTableAjaxPostModel tableViewModel, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (tableViewModel.search != null) ? tableViewModel.search.value : null;
            var take = tableViewModel.length;
            var skip = tableViewModel.start;

            string sortBy = "Id";
            bool sortAsc = true;

            if (tableViewModel.order != null)
            {
                //sort on the 1st column
                sortBy = tableViewModel.columns[tableViewModel.order[0].column].name.ToLower();
                sortAsc = tableViewModel.order[0].dir.ToLower() == "asc";
            }

            // search the database taking into consideration table sorting and paging
            var result = GetDataFromDatabase(searchBy, take, skip, sortBy, sortAsc, out filteredResultsCount, out totalResultsCount);
            if (result == null)
            {
                // empty collection...
                return new List<CustomerDto>();
            }
            return result;
        }

        private List<CustomerDto> GetDataFromDatabase(string searchBy, int take, int skip, string sortBy, bool sortAsc, out int filteredResultsCount, out int totalResultsCount)
        {
            // Create an uninitialized list of Customsers
            List<CustomerDto> result;

            // Apply pagination, sorting, and filtering for DataTable's data 
            var filterQueryResult = ApplyDatableFilters(searchBy, sortBy, sortAsc, skip, take, out filteredResultsCount);

            result = _context.Customers
                .AsNoTracking()
                .Include(m => m.MembershipType)
                .Where(m => filterQueryResult
                .Contains(m.Id))
                .Select(customer => new CustomerDto
                {
                    Name = customer.Name,
                    Id = customer.Id,
                    MembershipTypeId = customer.MembershipTypeId,
                    MembershipType = new MembershipTypeDto
                    {
                        Id = customer.MembershipType.Id,
                        Name = customer.MembershipType.Name
                    }
                })
                .ToList();

            totalResultsCount = _context.Customers.Count();
            return result;
        }

        private IQueryable<int> ApplyDatableFilters(string searchBy, string sortBy, bool sortAsc, int skip, int take, out int filteredResultsCount)
        {
            var query = _context.Customers.Include(mov => mov.MembershipType).AsNoTracking().Select(mov => new { Id = mov.Id, Name = mov.Name, MembershipType = new { Name = mov.MembershipType.Name } });
            IQueryable<int> result;

            // If DataTable search value is not empty
            if (!String.IsNullOrWhiteSpace(searchBy))
            {
                // filter customers based on the search value
                query = query.Where(mov => mov.Name.Contains(searchBy));
            }

            // get the count of all the filtered customers
            filteredResultsCount = query.Count();

            // Apply sorting
            switch (sortBy.ToLower())
            {
                // Sort by customer Name
                case "name":
                    query = sortAsc ? query.OrderBy(mov => mov.Name) : query.OrderByDescending(mov => mov.Name);
                    break;
                // Sort by membership type
                case "membershiptype.name":
                    query = sortAsc ? query.OrderBy(mov => mov.MembershipType.Name) : query.OrderByDescending(y => y.MembershipType.Name);
                    break;
                // Sort by customer Id
                default:
                    query = sortAsc ? query.OrderBy(mov => mov.Id) : query.OrderByDescending(y => y.Id);
                    break;
            }
            result = query.Select(y => y.Id).Skip(skip).Take(take);

            return result;
        }
    }
}
