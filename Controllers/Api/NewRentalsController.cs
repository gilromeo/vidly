﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;
using System.Data.Entity;

namespace Vidly.Controllers.Api
{
    public class NewRentalsController : ApiController
    {
        private ApplicationDbContext _context;
        public NewRentalsController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult NewRental(NewRentalDto newRentalDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var customer = _context.Customers.Single(c => c.Id == newRentalDto.CustomerId);
            var movies = _context.Movies.Where(m => newRentalDto.MovieIds.Contains(m.Id)).ToList();
            var payment = new Payment
            {
                Amount = newRentalDto.PaidAmount,
                DatePaid = DateTime.Today
            };

            var newRental = new Rental();
            newRental.Customer = customer;
            newRental.DateRented = DateTime.Now;
            newRental.Payment = payment;
            newRental.RentalDetails = new List<RentalDetail>();

            foreach (var movie in movies)
            {
                if (movie.NumberAvailable == 0)
                {
                    return BadRequest(string.Format("{0} is not available", movie.Name));
                }

                var rentalDetail = new RentalDetail
                {
                    Movie = movie
                };

                movie.NumberAvailable -= 1;
                customer.NumberOfCurrentRental += 1;
                newRental.RentalDetails.Add(rentalDetail);

                _context.Rentals.Add(newRental);
            }
            _context.SaveChanges();

            return Ok();
        }
    }
}
