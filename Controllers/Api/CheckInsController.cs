﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vidly.Models;
using System.Data.Entity;
using Vidly.Dtos;

namespace Vidly.Controllers.Api
{
    public class CheckInsController : ApiController
    {
        private ApplicationDbContext _context;
        public CheckInsController()
        {
            _context = new ApplicationDbContext();
        }

        //POST: /api/checkIns/
        public IHttpActionResult SaveCheckIn(CheckInDto checkInDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var rentals = _context.Rentals
                .Include(r => r.Customer)
                .Include(r => r.RentalDetails)
                    .Include("RentalDetails.Movie")
                        .Include("RentalDetails.Movie.Type")
                        .Include("RentalDetails.Movie.Genre")
                    .Include("RentalDetails.Penalty")
                        .Include("RentalDetails.Penalty.Payment")
                .Where(r => r.Id == checkInDto.RentalId)
                .Single();

            foreach (var rental in rentals.RentalDetails)
            {
                if (checkInDto.RentalIds.Contains(rental.IntPropAsPK)) {
                    // if rental has not yet returned
                    if (rental.DateReturned == null)
                    {
                        rental.DateReturned = DateTime.Now;
                        rental.Movie.NumberAvailable += 1;
                        rentals.Customer.NumberOfCurrentRental -= 1;

                        // check if has penalty
                        var dueDate = rentals.DateRented.AddDays(rental.Movie.Type.MaximumRentalPeriodInDays);
                        var penalty = GetPenalty(dueDate, rental.Movie.Type.LatePenaltyPerDay);

                        // if has penalty
                        if (penalty > 0)
                        {
                            //create new penalty object
                            rental.Penalty = new Penalty();
                            rental.Penalty.Amount = penalty;

                            // if penalty is paid
                            if (checkInDto.IsPaid)
                            {
                                //create payment for penalty
                                rental.Penalty.Payment = new Payment();
                                rental.Penalty.Payment.Amount = penalty;
                                rental.Penalty.Payment.DatePaid = DateTime.Today;
                            }
                            // if penalty is not paid
                            else
                            {
                                //mark customer as Delinquent
                                rentals.Customer.IsDelinquent = true;
                            }
                        }
                    }
                    // if rental has been returned but penalty was not paid (Delinquent customer) 
                    else
                    {
                        // if rental has referenced penalty 
                        if (rental.Penalty != null)
                        {
                            // if customer has paid the penalty just now
                            if (checkInDto.IsPaid)
                            {
                                // create payment for penalty and remove Delinquent flag on Customer
                                rental.Penalty.Payment = new Payment();
                                rental.Penalty.Payment.Amount = rental.Penalty.Amount;
                                rental.Penalty.Payment.DatePaid = DateTime.Today;

                                rentals.Customer.IsDelinquent = false;
                            }
                        }
                        else
                        {
                            //create payment for penalty
                            rental.Penalty.Payment = new Payment();
                            rental.Penalty.Payment.Amount = rental.Penalty.Amount;
                            rental.Penalty.Payment.DatePaid = DateTime.Today;
                        }

                    }
                }
            }

            bool isDelinquent = false;
            foreach (var rental in rentals.RentalDetails)
            {
                if (rental.Penalty != null)
                {
                    if (rental.Penalty.Payment == null)
                    {
                        isDelinquent = true;
                        break;
                    }
                }
            }
            rentals.Customer.IsDelinquent = isDelinquent;

            _context.SaveChanges();

            return Ok();
        }

        private decimal GetPenalty(DateTime dueDate, decimal penalty)
        {
            var today = DateTime.Today;
            if (today <= dueDate)
            {
                return 0;
            }

            return (today - dueDate).Days * penalty;
        }
    }
}