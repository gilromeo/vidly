﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Vidly.Dtos;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers.Api
{
    public class RentalsController : ApiController
    {
        private ApplicationDbContext _context;
        public RentalsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: /api/rentals
        public IHttpActionResult GetRentals()
        {
            var dbRentals = _context.Rentals.Include(r => r.Customer).ToList();
            var rentals = new List<RentalDto>();

            //Map Rental object to RentalDto object
            foreach (var dbRental in dbRentals)
            {
                var rentalDto = new RentalDto
                {
                    Id = dbRental.Id,
                    CustomerName = dbRental.Customer.Name,
                    DateRented = dbRental.DateRented
                };

                rentals.Add(rentalDto);
            }

            return Ok(rentals);
        }

        //GET: /api/rentals/{id}
        public IHttpActionResult GetRental(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var dbRental = _context.Rentals
                .Include(r => r.Customer)
                .Include(r => r.RentalDetails)
                    .Include("RentalDetails.Movie")
                        .Include("RentalDetails.Movie.Type")
                        .Include("RentalDetails.Movie.Genre")
                    .Include("RentalDetails.Penalty")
                .Where(r => r.Id == id)
                .Single();

            var rental = new RentalDto
            {
                DateRented = dbRental.DateRented,
                CustomerName = dbRental.Customer.Name,
                RentalDetails = new List<RentalDetailDto>()
            };

            foreach (var dbRentalDetail in dbRental.RentalDetails)
            {
                var rentalDetail = new RentalDetailDto();

                rentalDetail.Id = dbRentalDetail.IntPropAsPK;
                rentalDetail.DateRented = dbRental.DateRented;
                rentalDetail.DateReturned = dbRentalDetail.DateReturned;
                rentalDetail.MovieName = dbRentalDetail.Movie.Name;
                rentalDetail.DueDate = dbRental.DateRented.AddDays(dbRentalDetail.Movie.Type.MaximumRentalPeriodInDays);
                rentalDetail.Penalty = GetPenalty(rentalDetail.DueDate, dbRentalDetail.Movie.Type.LatePenaltyPerDay);

                rental.RentalDetails.Add(rentalDetail);
            }

            return Ok(rental);
        }

        //POST: /api/rentals/rentals-datatable
        [HttpPost]
        [Route("api/rentals/rentals-datatable")]
        public IHttpActionResult GetDataTableRentals(DataTableAjaxPostModel tableViewModel)
        {
            _context.Configuration.AutoDetectChangesEnabled = false;

            int filteredResultsCount;
            int totalResultsCount;
            var res = GetDataTableData(tableViewModel, out filteredResultsCount, out totalResultsCount);

            return Json(
                new
                {
                    draw = tableViewModel.draw,
                    recordsTotal = totalResultsCount,
                    recordsFiltered = filteredResultsCount,
                    data = res
                });
        }

        // Private Functions
        private IList<RentalDto> GetDataTableData(DataTableAjaxPostModel tableViewModel, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (tableViewModel.search != null) ? tableViewModel.search.value : null;
            var take = tableViewModel.length;
            var skip = tableViewModel.start;

            string sortBy = "Id";
            bool sortAsc = true;

            if (tableViewModel.order != null)
            {
                //sort on the 1st column
                sortBy = tableViewModel.columns[tableViewModel.order[0].column].name.ToLower();
                sortAsc = tableViewModel.order[0].dir.ToLower() == "asc";
            }

            // search the database taking into consideration table sorting and paging
            var result = GetDataFromDatabase(searchBy, take, skip, sortBy, sortAsc, out filteredResultsCount, out totalResultsCount);
            if (result == null)
            {
                // empty collection...
                return new List<RentalDto>();
            }
            return result;
        }

        private List<RentalDto> GetDataFromDatabase(string searchBy, int take, int skip, string sortBy, bool sortAsc, out int filteredResultsCount, out int totalResultsCount)
        {
            // Create an uninitialized list of Movies
            List<RentalDto> result;

            // Apply pagination, sorting, and filtering for DataTable's data 
            var filterQueryResult = ApplyDatableFilters(searchBy, sortBy, sortAsc, skip, take, out filteredResultsCount);
            //grm

            result = _context.Rentals
                .AsNoTracking()
                .Include(r => r.Customer)
                .Where(r => filterQueryResult
                .Contains(r.Id))
                .Select(dbRental => new RentalDto
                {
                    Id = dbRental.Id,
                    CustomerName = dbRental.Customer.Name,
                    DateRented = dbRental.DateRented
                })
                .ToList();

            totalResultsCount = _context.Rentals.Count();
            return result;
        }

        // Private Functions
        private IQueryable<int> ApplyDatableFilters(string searchBy, string sortBy, bool sortAsc, int skip, int take, out int filteredResultsCount)
        {
            var query = _context.Rentals.Include(r => r.Customer).AsNoTracking();
            IQueryable<int> result;

            // If DataTable search value is not empty
            if (!String.IsNullOrWhiteSpace(searchBy))
            {
                // filter movies based on the search value
                query = query.Where(r => r.Customer.Name.Contains(searchBy));
            }

            // get the count of all the filtered movies
            filteredResultsCount = query.Count();

            // Apply sorting
            switch (sortBy)
            {
                // Sort by Movie Name
                case "customername":
                    query = sortAsc ? query.OrderBy(r => r.Customer.Name) : query.OrderByDescending(r => r.Customer.Name);
                    break;
                // Sort by Genre Name
                case "daterented":
                    query = sortAsc ? query.OrderBy(r => r.DateRented) : query.OrderByDescending(r => r.DateRented);
                    break;
                // Sort by Movie Id
                default:
                    query = sortAsc ? query.OrderBy(r => r.Id) : query.OrderByDescending(r => r.Id);
                    break;
            }
            result = query.Select(y => y.Id).Skip(skip).Take(take);

            return result;
        }

        private decimal GetPenalty(DateTime dueDate, decimal penalty)
        {
            var today = DateTime.Today;
            if (today <= dueDate)
            {
                return 0;
            }
            return (today - dueDate).Days * penalty;
        }
    }
}