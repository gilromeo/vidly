﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Vidly.Dtos;
using Vidly.Models;
using Vidly.ViewModels;
using System.Data.Entity;

namespace Vidly.Controllers.ApiExtJs
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class MoviesExtJsController : ApiController
    {
        private ApplicationDbContext _context;

        public MoviesExtJsController()
        {
            _context = new ApplicationDbContext();
        }

        //GET: /api/CustomersExtJs/
        [HttpGet]
        public IHttpActionResult GetList(int page, int start, int limit, [FromUri] object sort, [FromUri] object filter)
        {
            // Get ExtJs Filters
            var extJsGridFilters = GetExtJsGridFilters(page, start, limit, sort, filter);

            // Apply ExtJs grid filters 
            var movies = GetExtJsDataFromDatabase(extJsGridFilters, out int totalResultsCount);

            return Ok(new { data = movies, total = totalResultsCount, success = true });
        }

        //GET: /api/CustomersExtJs/{id}
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return NotFound();
        }

        //POST: /api/CustomersExtJs/
        [HttpPost]
        public IHttpActionResult Post(MovieDto movieDto)
        {
            var newMovie = new Movie
            {
                Name = movieDto.Name,
                GenreId = movieDto.GenreId,
                ReleasedDate = movieDto.ReleasedDate,
                DateAdded = DateTime.Today,
                InStock = movieDto.InStock,
                NumberAvailable = movieDto.InStock,
                MovieTypeId = movieDto.MovieTypeId,
            };

            _context.Movies.Add(newMovie);
            _context.SaveChanges();

            return Ok(newMovie);
        }

        //PUT: /api/CustomersExtJs/
        [HttpPut]
        public IHttpActionResult Put(int id, MovieDto movieDto)
        {
            var movie = _context.Movies.Where(c => c.Id == id).Single();

            if (movie == null)
            {
                return NotFound();
            }

            movie.Name = movieDto.Name;
            movie.ReleasedDate = movieDto.ReleasedDate;
            movie.MovieTypeId = movieDto.MovieTypeId;
            movie.GenreId = movieDto.GenreId;
            movie.InStock = movieDto.InStock;


            _context.SaveChanges();

            return Ok();
        }


        //DELETE: /api/CustomersExtJs/{id}
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var MovieToBeDeleted = _context.Movies.Where(c => c.Id == id).Single();

            if (MovieToBeDeleted == null)
            {
                return NotFound();
            }

            _context.Movies.Remove(MovieToBeDeleted);
            _context.SaveChanges();

            return Ok(MovieToBeDeleted);
        }


        // Private Functions
        // ExtJS 
        private ExtJsGridFiltersViewModel GetExtJsGridFilters(int page, int start, int limit, object sort, object filter)
        {
            var result = new ExtJsGridFiltersViewModel();

            result.Take = limit;
            result.Skip = start;
            result.Sort = sort != null ? JsonConvert.DeserializeObject<ExtJsSortViewModel[]>((string)sort)[0] : null;

            try
            {
                result.Search = filter != null ? JsonConvert.DeserializeObject<ExtJsSearchViewModel[]>((string)filter)[0] : null;
            }
            catch (Exception)
            {
                result.Search = null;
            }

            return result;
        }

        private List<MovieDto> GetExtJsDataFromDatabase(ExtJsGridFiltersViewModel extjsFilters, out int totalResultsCount)
        {
            // Create an uninitialized list of Movies
            List<MovieDto> result;

            // Apply pagination, sorting, and filtering for DataTable's data 
            var filterQueryResult = ApplyExtJsDatableFilters(extjsFilters, out totalResultsCount);

            result = _context.Movies
                .AsNoTracking()
                .Include(m => m.Genre)
                .Where(m => filterQueryResult
                .Contains(m.Id))
                .Select(movie => new MovieDto
                {
                    Name = movie.Name,
                    Id = movie.Id,
                    ReleasedDate = movie.ReleasedDate,
                    DateAdded = movie.DateAdded,
                    InStock = movie.InStock,
                    NumberAvailable = movie.NumberAvailable,
                    GenreId = movie.GenreId,
                    MovieTypeId = movie.MovieTypeId,
                    GenreName = movie.Genre.Name,
                    MovieTypeName = movie.Type.Name
                })
                .ToList();

            return result;
        }

        private IQueryable<int> ApplyExtJsDatableFilters(ExtJsGridFiltersViewModel extjsFilters, out int totalResultsCount)
        {
            var query = _context.Movies.Include(mov => mov.Genre).AsNoTracking().Select(mov => new { Id = mov.Id, Name = mov.Name, Genre = new { Name = mov.Genre.Name } });
            IQueryable<int> result;
            string sortBy = extjsFilters.Sort.property.ToLower();
            bool sortAsc = extjsFilters.Sort.direction.ToLower() == "asc" ? true : false;

            // If DataTable search value is not empty
            if (extjsFilters.Search != null)
            {
                if (!String.IsNullOrWhiteSpace(extjsFilters.Search.value))
                {
                    // filter movies based on the search value
                    query = query.Where(mov => mov.Name.Contains(extjsFilters.Search.value));
                }
            }

            // get the count of all the filtered movies
            totalResultsCount = query.Count();

            // Apply sorting
            switch (sortBy)
            {
                // Sort by movies Name
                case "name":
                    query = sortAsc ? query.OrderBy(mov => mov.Name) : query.OrderByDescending(mov => mov.Name);
                    break;
                // Sort by genre type
                case "genrename":
                    query = sortAsc ? query.OrderBy(mov => mov.Genre.Name) : query.OrderByDescending(y => y.Genre.Name);
                    break;
                // Sort by movies Id
                default:
                    query = sortAsc ? query.OrderBy(mov => mov.Id) : query.OrderByDescending(y => y.Id);
                    break;
            }
            result = query.Select(y => y.Id).Skip(extjsFilters.Skip).Take(extjsFilters.Take);

            return result;
        }
    }
}