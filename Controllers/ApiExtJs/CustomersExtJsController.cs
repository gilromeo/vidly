﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data.Entity;
using Vidly.Dtos;
using Vidly.Models;
using Vidly.ViewModels;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Vidly.Controllers.ApiExtJs
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class CustomersExtJsController : ApiController
    {
        private ApplicationDbContext _context;

        public CustomersExtJsController()
        {
            _context = new ApplicationDbContext();
        }

        //GET: /api/CustomersExtJs/
        [HttpGet]
        public IHttpActionResult GetList(int page, int start, int limit, [FromUri] object sort, [FromUri] object filter)
        {
            // Get ExtJs Filters
            var extJsGridFilters = GetExtJsGridFilters(page, start, limit, sort, filter);

            // Apply ExtJs grid filters 
            var customers = GetExtJsDataFromDatabase(extJsGridFilters, out int totalResultsCount);

            return Ok(new { data = customers, total = totalResultsCount, success = true });
        }

        //GET: /api/CustomersExtJs/{id}
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return NotFound();
        }

        //POST: /api/CustomersExtJs/
        [HttpPost]
        public IHttpActionResult Post(CustomerDto customerDto)
        {
            var newCustomer = new Customer
            {
                Name = customerDto.Name,
                Birthday = customerDto.Birthday,
                IsSubscribedToNewsletter = customerDto.IsSubscribedToNewsletter,
                IsDelinquent = false,
                MembershipTypeId = customerDto.MembershipTypeId,
                NumberOfAllowableRental = customerDto.NumberOfAllowableRental,
                NumberOfCurrentRental = 0,
                DateAdded = DateTime.Today,
                DateRegistered = DateTime.Today
            };

            _context.Customers.Add(newCustomer);
            _context.SaveChanges();

            return Ok(newCustomer);
        }

        //PUT: /api/CustomersExtJs/
        [HttpPut]
        public IHttpActionResult Put(int id, CustomerDto customerDto)
        {
            var customer = _context.Customers.Where(c => c.Id == id).Single();

            if (customer == null)
            {
                return NotFound();
            }

            customer.Name = customerDto.Name;
            customer.Birthday = customerDto.Birthday;
            customer.IsSubscribedToNewsletter = customerDto.IsSubscribedToNewsletter;
            customer.MembershipTypeId = customerDto.MembershipTypeId;
            customer.NumberOfAllowableRental = customerDto.NumberOfAllowableRental;


            _context.SaveChanges();

            return Ok();
        }


        //DELETE: /api/CustomersExtJs/{id}
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var customerToBeDeleted = _context.Customers.Where(c => c.Id == id).Single();

            if (customerToBeDeleted == null) {
                return NotFound();
            }

            _context.Customers.Remove(customerToBeDeleted);
            _context.SaveChanges();

            return Ok(customerToBeDeleted);
        }


        // Private Functions
        // ExtJS 
        private ExtJsGridFiltersViewModel GetExtJsGridFilters(int page, int start, int limit, object sort, object filter)
        {
            var result = new ExtJsGridFiltersViewModel();

            result.Take = limit;
            result.Skip = start;
            result.Sort = sort != null ? JsonConvert.DeserializeObject<ExtJsSortViewModel[]>((string)sort)[0] : null;

            try
            {
                result.Search = filter != null ? JsonConvert.DeserializeObject<ExtJsSearchViewModel[]>((string)filter)[0] : null;
            }
            catch (Exception)
            {
                result.Search = null;
            }

            return result;
        }

        private List<CustomerDto> GetExtJsDataFromDatabase(ExtJsGridFiltersViewModel extjsFilters, out int totalResultsCount)
        {
            // Create an uninitialized list of Customsers
            List<CustomerDto> result;

            // Apply pagination, sorting, and filtering for DataTable's data 
            var filterQueryResult = ApplyExtJsDatableFilters(extjsFilters, out totalResultsCount);

            result = _context.Customers
                .AsNoTracking()
                .Include(m => m.MembershipType)
                .Where(m => filterQueryResult
                .Contains(m.Id))
                .Select(customer => new CustomerDto
                {
                    Name = customer.Name,
                    Id = customer.Id,
                    IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter,
                    MembershipTypeId = customer.MembershipTypeId,
                    Discount = customer.MembershipType.DiscountRate,
                    Birthday = customer.Birthday,
                    NumberOfAllowableRental = customer.NumberOfAllowableRental,
                    NumberOfCurrentRental = customer.NumberOfCurrentRental,
                    IsDelinquent = customer.IsDelinquent,
                    MembershipTypeName = customer.MembershipType.Name,
                    MembershipType = new MembershipTypeDto
                    {
                        Id = customer.MembershipType.Id,
                        Name = customer.MembershipType.Name
                    }
                })
                .ToList();

            return result;
        }

        private IQueryable<int> ApplyExtJsDatableFilters(ExtJsGridFiltersViewModel extjsFilters, out int totalResultsCount)
        {
            var query = _context.Customers.Include(mov => mov.MembershipType).AsNoTracking().Select(mov => new { Id = mov.Id, Name = mov.Name, MembershipType = new { Name = mov.MembershipType.Name } });
            IQueryable<int> result;
            string sortBy = extjsFilters.Sort.property.ToLower();
            bool sortAsc = extjsFilters.Sort.direction.ToLower() == "asc" ? true : false;

            // If DataTable search value is not empty
            if (extjsFilters.Search != null)
            {
                if (!String.IsNullOrWhiteSpace(extjsFilters.Search.value))
                {
                    // filter customers based on the search value
                    query = query.Where(mov => mov.Name.Contains(extjsFilters.Search.value));
                }
            }

            // get the count of all the filtered customers
            totalResultsCount = query.Count();

            // Apply sorting
            switch (sortBy)
            {
                // Sort by customer Name
                case "name":
                    query = sortAsc ? query.OrderBy(mov => mov.Name) : query.OrderByDescending(mov => mov.Name);
                    break;
                // Sort by membership type
                case "membershiptypename":
                    query = sortAsc ? query.OrderBy(mov => mov.MembershipType.Name) : query.OrderByDescending(y => y.MembershipType.Name);
                    break;
                // Sort by customer Id
                default:
                    query = sortAsc ? query.OrderBy(mov => mov.Id) : query.OrderByDescending(y => y.Id);
                    break;
            }
            result = query.Select(y => y.Id).Skip(extjsFilters.Skip).Take(extjsFilters.Take);

            return result;
        }
    }
}