﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidly.Models;
using System.Web.Http.Cors;

namespace Vidly.Controllers.ApiExtJs
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class MembershipTypeExtJsController : ApiController
    {
        private ApplicationDbContext _context;

        public MembershipTypeExtJsController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpGet]
        public IHttpActionResult GetList()
        {
            return Ok(_context.MembershipTypes.ToList());
        }
    }
}