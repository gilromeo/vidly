﻿using System.Linq;
using System.Web.Http;
using Vidly.Models;
using System.Web.Http.Cors;

namespace Vidly.Controllers.ApiExtJs
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    public class GenreExtJsController : ApiController
    {
        private ApplicationDbContext _context;

        public GenreExtJsController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpGet]
        public IHttpActionResult GetList()
        {
            return Ok(_context.Genres.ToList());
        }
    }
}