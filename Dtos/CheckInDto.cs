﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.Dtos
{
    public class CheckInDto
    {
        public int Id { get; set; }

        public string MovieName { get; set; }

        public string CustomerName { get; set; }

        public int RentalId { get; set; }

        public int[] RentalIds { get; set; }

        public DateTime DateRented { get; set; }

        public DateTime? DateReturned { get; set; }

        public DateTime DueDate { get; set; }

        public decimal Penalty { get; set; }

        public MovieTypeDto MovieTypeDto { get; set; }

        public bool IsPaid { get; set; }
    }
}