﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Vidly.Models;
using Vidly.Models.Custom_Validators;

namespace Vidly.Dtos
{
    public class CustomerDto
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [MembershipAgeRequirement]
        public DateTime? Birthday { get; set; }

        public bool IsSubscribedToNewsletter { get; set; }

        public byte MembershipTypeId { get; set; }

        public string MembershipTypeName { get; set; }

        public byte NumberOfAllowableRental { get; set; }

        public byte NumberOfCurrentRental { get; set; }

        public byte Discount { get; set; }

        public MembershipTypeDto MembershipType { get; set; }

        public bool IsDelinquent { get; set; }
    }
}