﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Dtos
{
    public class NewRentalDto
    {
        public int CustomerId { get; set; }
        
        [Range(1, 10)]
        public byte NumberOfAllowableRental { get; set; }
        
        public byte NumberOfCurrentRental { get; set; }

        public List<int> MovieIds { get; set; }

        [Required]
        public decimal PaidAmount { get; set; }
    }
}