﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Dtos
{
    public class MovieTypeDto
    {
        public byte Id { get; set; }

        public string Name { get; set; }

        public decimal Rate { get; set; }

        public decimal LatePenaltyPerDay { get; set; }

        public byte MaximumRentalPeriodInDays { get; set; }
    }
}