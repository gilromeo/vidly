﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Dtos
{
    public class MovieDto
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public byte GenreId { get; set; }

        public byte NumberAvailable { get; set; }

        [Required]
        public DateTime ReleasedDate { get; set; }

        [Required]
        public DateTime DateAdded { get; set; }

        [Required]
        [Range(1, 20)]
        public byte InStock { get; set; }

        public GenreDto Genre { get; set; }

        public byte MovieTypeId { get; set; }

        public MovieTypeDto MovieType { get; set; }

        public string GenreName { get; set; }

        public string MovieTypeName { get; set; }
    }
}