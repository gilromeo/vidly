﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.Dtos
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public List<string> Roles { get; set; }

        public byte RoleId { get; set; }

        public string DrivingLicense { get; set; }

        public string Phone { get; set; }
    }
}