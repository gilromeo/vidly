﻿using System;
using System.Collections.Generic;

namespace Vidly.Dtos
{
    public class RentalDto
    {
        public int Id { get; set; }

        public DateTime DateRented { get; set; }

        public string CustomerName { get; set; }

        public virtual List<RentalDetailDto> RentalDetails { get; set; }
    }
}