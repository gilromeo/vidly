﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.Dtos
{
    public class RentalDetailDto
    {
        public int Id { get; set; }

        public DateTime? DateReturned { get; set; }

        public DateTime DateRented { get; set; }

        public DateTime DueDate { get; set; }

        public string MovieName { get; set; }

        public decimal Penalty { get; set; }
    }
}