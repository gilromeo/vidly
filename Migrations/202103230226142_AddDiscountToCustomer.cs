namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDiscountToCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "Discount", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "Discount");
        }
    }
}
