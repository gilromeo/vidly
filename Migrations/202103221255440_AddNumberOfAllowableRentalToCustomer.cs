namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNumberOfAllowableRentalToCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "NumberOfAllowableRental", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Customers", "NumberOfAllowableRental");
        }
    }
}
