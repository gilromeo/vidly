namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMemberSignupAndPenalty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Invoices", "InvoiceTypeId", "dbo.InvoiceTypes");
            DropForeignKey("dbo.Invoices", "PaymentId", "dbo.Payments");
            DropForeignKey("dbo.Rentals", "InvoiceId", "dbo.Invoices");
            DropIndex("dbo.Rentals", new[] { "InvoiceId" });
            DropIndex("dbo.Invoices", new[] { "PaymentId" });
            DropIndex("dbo.Invoices", new[] { "InvoiceTypeId" });
            CreateTable(
                "dbo.Penalties",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DatePaid = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MemberSignUps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaymentId = c.Int(),
                        CustomerId = c.Int(nullable: false),
                        MembershipTypeId = c.Int(nullable: false),
                        MembershipType_Id = c.Byte(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .ForeignKey("dbo.MembershipTypes", t => t.MembershipType_Id)
                .ForeignKey("dbo.Payments", t => t.PaymentId)
                .Index(t => t.PaymentId)
                .Index(t => t.CustomerId)
                .Index(t => t.MembershipType_Id);
            
            AddColumn("dbo.Customers", "DateAdded", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "DateRegistered", c => c.DateTime());
            AddColumn("dbo.Rentals", "PaymentId", c => c.Int(nullable: false));
            AddColumn("dbo.Rentals", "PenaltyId", c => c.Int());
            CreateIndex("dbo.Rentals", "PaymentId");
            CreateIndex("dbo.Rentals", "PenaltyId");
            AddForeignKey("dbo.Rentals", "PaymentId", "dbo.Payments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Rentals", "PenaltyId", "dbo.Penalties", "Id");
            DropColumn("dbo.Rentals", "InvoiceId");
            DropTable("dbo.Invoices");
            DropTable("dbo.InvoiceTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.InvoiceTypes",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaymentId = c.Int(nullable: false),
                        InvoiceTypeId = c.Byte(nullable: false),
                        DateIssued = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Rentals", "InvoiceId", c => c.Int(nullable: false));
            DropForeignKey("dbo.MemberSignUps", "PaymentId", "dbo.Payments");
            DropForeignKey("dbo.MemberSignUps", "MembershipType_Id", "dbo.MembershipTypes");
            DropForeignKey("dbo.MemberSignUps", "CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Rentals", "PenaltyId", "dbo.Penalties");
            DropForeignKey("dbo.Rentals", "PaymentId", "dbo.Payments");
            DropIndex("dbo.MemberSignUps", new[] { "MembershipType_Id" });
            DropIndex("dbo.MemberSignUps", new[] { "CustomerId" });
            DropIndex("dbo.MemberSignUps", new[] { "PaymentId" });
            DropIndex("dbo.Rentals", new[] { "PenaltyId" });
            DropIndex("dbo.Rentals", new[] { "PaymentId" });
            DropColumn("dbo.Rentals", "PenaltyId");
            DropColumn("dbo.Rentals", "PaymentId");
            DropColumn("dbo.Customers", "DateRegistered");
            DropColumn("dbo.Customers", "DateAdded");
            DropTable("dbo.MemberSignUps");
            DropTable("dbo.Penalties");
            CreateIndex("dbo.Invoices", "InvoiceTypeId");
            CreateIndex("dbo.Invoices", "PaymentId");
            CreateIndex("dbo.Rentals", "InvoiceId");
            AddForeignKey("dbo.Rentals", "InvoiceId", "dbo.Invoices", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Invoices", "PaymentId", "dbo.Payments", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Invoices", "InvoiceTypeId", "dbo.InvoiceTypes", "Id", cascadeDelete: true);
        }
    }
}
