namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedDiscountFromCustomer : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Customers", "Discount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "Discount", c => c.Byte(nullable: false));
        }
    }
}
