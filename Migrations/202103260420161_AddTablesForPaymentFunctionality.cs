namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTablesForPaymentFunctionality : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PaymentId = c.Int(nullable: false),
                        InvoiceTypeId = c.Byte(nullable: false),
                        DateIssued = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InvoiceTypes", t => t.InvoiceTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Payments", t => t.PaymentId, cascadeDelete: true)
                .Index(t => t.PaymentId)
                .Index(t => t.InvoiceTypeId);
            
            CreateTable(
                "dbo.InvoiceTypes",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DatePaid = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Rentals", "InvoiceId", c => c.Int(nullable: false));
            CreateIndex("dbo.Rentals", "InvoiceId");
            AddForeignKey("dbo.Rentals", "InvoiceId", "dbo.Invoices", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rentals", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "PaymentId", "dbo.Payments");
            DropForeignKey("dbo.Invoices", "InvoiceTypeId", "dbo.InvoiceTypes");
            DropIndex("dbo.Invoices", new[] { "InvoiceTypeId" });
            DropIndex("dbo.Invoices", new[] { "PaymentId" });
            DropIndex("dbo.Rentals", new[] { "InvoiceId" });
            DropColumn("dbo.Rentals", "InvoiceId");
            DropTable("dbo.Payments");
            DropTable("dbo.InvoiceTypes");
            DropTable("dbo.Invoices");
        }
    }
}
