namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedFluentAPI : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Penalties", "RentalDetailId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Penalties", "RentalDetailId");
        }
    }
}
