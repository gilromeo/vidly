namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteModels : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Giromes");
            DropTable("dbo.Pogis");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Pogis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GiromeId = c.Int(nullable: false),
                        PogiName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Giromes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PogiId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
