namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDataTypeOfCustomerNameAndMovieName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Customers", "FullName", c => c.String(nullable: false, maxLength: 255, unicode: false));
            AlterColumn("dbo.Movies", "Name", c => c.String(nullable: false, maxLength: 255, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Movies", "Name", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Customers", "FullName", c => c.String(nullable: false, maxLength: 255));
        }
    }
}
