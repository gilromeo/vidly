namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'171618d3-ca66-48cb-b920-8f7ffc85e495', N'guest@vidly.com', 0, N'AG9IHxpvNkeRCGiZhpFGRvXtjVH3p9atwKiX6rdwwiBprwD3fmaE9rtno1ynFCu9Qw==', N'82b0369c-7591-4a08-b4c9-66e85ce02c3e', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'4d4b2696-d422-43fc-bcfe-977529ca4810', N'girome.andaya@gmail.com', 0, N'ACpmkZVqHcNsqyunBn7zTrbps37WK82dTRaWSUuo34e9S1YzgAG74hBkOnJ4r7YDGQ==', N'8e25b79e-b3b2-4cb4-8809-b65b5d67a53b', NULL, 0, 0, NULL, 1, 0, N'girome.andaya@gmail.com')
                INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e592d6a7-3b78-4652-b42a-ecf7b82616c8', N'admin@vidly.com', 0, N'AEXlLxj9d6o2UQsDDAPQ32UBkeW4aRnAJPPzyH6mg8FLNZuuUCoqWVxjR+wKMfLkAw==', N'e5782df2-6e2f-460c-8f6b-471aa84e204c', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')

                INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'361ed0f4-ddee-42d0-9307-fcb1c54ee4f5', N'CanManageMovies')

                INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e592d6a7-3b78-4652-b42a-ecf7b82616c8', N'361ed0f4-ddee-42d0-9307-fcb1c54ee4f5')
            ");


        }

        public override void Down()
        {
        }
    } 
}
