namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class PopulateMovieTypes : DbMigration
    {
        public override void Up()
        {
            Sql(@"SET IDENTITY_INSERT [dbo].[MovieTypes] ON
            INSERT INTO [dbo].[MovieTypes] ([Id], [Name], [Rate], [LatePenaltyPerDay], [MaximumRentalPeriodInDays]) VALUES (1, N'Latest', CAST(50.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), 2)
            INSERT INTO [dbo].[MovieTypes] ([Id], [Name], [Rate], [LatePenaltyPerDay], [MaximumRentalPeriodInDays]) VALUES (2, N'Old', CAST(50.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), 5)
            INSERT INTO [dbo].[MovieTypes] ([Id], [Name], [Rate], [LatePenaltyPerDay], [MaximumRentalPeriodInDays]) VALUES (3, N'Exclusive', CAST(70.00 AS Decimal(18, 2)), CAST(20.00 AS Decimal(18, 2)), 5)
            SET IDENTITY_INSERT [dbo].[MovieTypes] OFF");
        }

        public override void Down()
        {
        }
    }
}
