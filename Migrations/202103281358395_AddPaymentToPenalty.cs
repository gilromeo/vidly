namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentToPenalty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Penalties", "PaymentId", c => c.Int());
            CreateIndex("dbo.Penalties", "PaymentId");
            AddForeignKey("dbo.Penalties", "PaymentId", "dbo.Payments", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Penalties", "PaymentId", "dbo.Payments");
            DropIndex("dbo.Penalties", new[] { "PaymentId" });
            DropColumn("dbo.Penalties", "PaymentId");
        }
    }
}
