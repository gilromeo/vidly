namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovePenaltyId : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.RentalDetails", "PenaltyId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RentalDetails", "PenaltyId", c => c.Int(nullable: false));
        }
    }
}
