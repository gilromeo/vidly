namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeMovieNameToMovieTitle : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Movies", name: "Name", newName: "Title");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Movies", name: "Title", newName: "Name");
        }
    }
}
