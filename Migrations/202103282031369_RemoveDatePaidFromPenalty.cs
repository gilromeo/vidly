namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveDatePaidFromPenalty : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Penalties", "DatePaid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Penalties", "DatePaid", c => c.DateTime(nullable: false));
        }
    }
}
