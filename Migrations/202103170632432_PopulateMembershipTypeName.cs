namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMembershipTypeName : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE MembershipTypes Set Name = 'Pay As You Go' WHERE MembershipTypes.Id = 1");
            Sql("UPDATE MembershipTypes Set Name = 'Monthly' WHERE MembershipTypes.Id = 2");
            Sql("UPDATE MembershipTypes Set Name = 'Quarterly' WHERE MembershipTypes.Id = 3");
            Sql("UPDATE MembershipTypes Set Name = 'Annual' WHERE MembershipTypes.Id = 4");
        }
        
        public override void Down()
        {
        }
    }
}
