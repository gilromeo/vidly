namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTestTables1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Giromes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PogiId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pogis",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GiromeId = c.Int(nullable: false),
                        PogiName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pogis");
            DropTable("dbo.Giromes");
        }
    }
}
