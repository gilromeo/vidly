namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRentalDetail : DbMigration
    {
        public override void Up()
        {

            DropTable("dbo.Students");
            DropTable("dbo.Courses");
            DropPrimaryKey("dbo.Penalties");
            CreateTable(
                "dbo.RentalDetails",
                c => new
                    {
                        IntPropAsPK = c.Int(nullable: false, identity: true),
                        DateReturned = c.DateTime(),
                        IntPropAsMovieFK = c.Int(nullable: false),
                        PenaltyId = c.Int(nullable: false),
                        Rental_Id = c.Int(),
                    })
                .PrimaryKey(t => t.IntPropAsPK)
                .ForeignKey("dbo.Movies", t => t.IntPropAsMovieFK, cascadeDelete: true)
                .ForeignKey("dbo.Rentals", t => t.Rental_Id)
                .Index(t => t.IntPropAsMovieFK)
                .Index(t => t.Rental_Id);
            
            AddColumn("dbo.Penalties", "RentalDetail_IntPropAsPK", c => c.Int());
            AlterColumn("dbo.Penalties", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Penalties", "Id");
            CreateIndex("dbo.Penalties", "Id");
            CreateIndex("dbo.Penalties", "RentalDetail_IntPropAsPK");
            AddForeignKey("dbo.Penalties", "Id", "dbo.RentalDetails", "IntPropAsPK");
            AddForeignKey("dbo.Penalties", "RentalDetail_IntPropAsPK", "dbo.RentalDetails", "IntPropAsPK");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RentalDetails", "Rental_Id", "dbo.Rentals");
            DropForeignKey("dbo.Penalties", "RentalDetail_IntPropAsPK", "dbo.RentalDetails");
            DropForeignKey("dbo.Penalties", "Id", "dbo.RentalDetails");
            DropForeignKey("dbo.RentalDetails", "IntPropAsMovieFK", "dbo.Movies");
            DropIndex("dbo.RentalDetails", new[] { "Rental_Id" });
            DropIndex("dbo.RentalDetails", new[] { "IntPropAsMovieFK" });
            DropIndex("dbo.Penalties", new[] { "RentalDetail_IntPropAsPK" });
            DropIndex("dbo.Penalties", new[] { "Id" });
            DropPrimaryKey("dbo.Penalties");
            AlterColumn("dbo.Penalties", "Id", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Penalties", "RentalDetail_IntPropAsPK");
            DropTable("dbo.RentalDetails");
            AddPrimaryKey("dbo.Penalties", "Id");
        }
    }
}
