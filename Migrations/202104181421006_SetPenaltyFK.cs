namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetPenaltyFK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Penalties", "RentalDetail_IntPropAsPK", "dbo.RentalDetails");
            DropIndex("dbo.Penalties", new[] { "RentalDetail_IntPropAsPK" });
            RenameColumn(table: "dbo.Penalties", name: "RentalDetail_IntPropAsPK", newName: "SomeIntPropForRentalDetailFK");
            AlterColumn("dbo.Penalties", "SomeIntPropForRentalDetailFK", c => c.Int(nullable: false));
            CreateIndex("dbo.Penalties", "SomeIntPropForRentalDetailFK");
            AddForeignKey("dbo.Penalties", "SomeIntPropForRentalDetailFK", "dbo.RentalDetails", "IntPropAsPK", cascadeDelete: true);
            DropColumn("dbo.Penalties", "RentalDetailId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Penalties", "RentalDetailId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Penalties", "SomeIntPropForRentalDetailFK", "dbo.RentalDetails");
            DropIndex("dbo.Penalties", new[] { "SomeIntPropForRentalDetailFK" });
            AlterColumn("dbo.Penalties", "SomeIntPropForRentalDetailFK", c => c.Int());
            RenameColumn(table: "dbo.Penalties", name: "SomeIntPropForRentalDetailFK", newName: "RentalDetail_IntPropAsPK");
            CreateIndex("dbo.Penalties", "RentalDetail_IntPropAsPK");
            AddForeignKey("dbo.Penalties", "RentalDetail_IntPropAsPK", "dbo.RentalDetails", "IntPropAsPK");
        }
    }
}
