namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMappingToFullNameColumnForCustomer : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Customers", name: "Name", newName: "FullName");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Customers", name: "FullName", newName: "Name");
        }
    }
}
