namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveMaximumRentalPeriodInDaysFromMovieToMovieType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MovieTypes", "MaximumRentalPeriodInDays", c => c.Byte(nullable: false));
            DropColumn("dbo.Movies", "MaximumRentalPeriodInDays");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Movies", "MaximumRentalPeriodInDays", c => c.Byte(nullable: false));
            DropColumn("dbo.MovieTypes", "MaximumRentalPeriodInDays");
        }
    }
}
