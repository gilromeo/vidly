namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyMovieId : DbMigration
    {
        public override void Up()
        {
            //RenameColumn(table: "dbo.Movies", name: "Id", newName: "Id2");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Movies", name: "Id2", newName: "Id");
        }
    }
}
