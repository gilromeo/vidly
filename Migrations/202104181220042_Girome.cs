namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Girome : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.Rentals", "Movie_Id", "dbo.Movies");
            //DropForeignKey("dbo.Rentals", "PenaltyId", "dbo.Penalties");
            ////DropForeignKey("dbo.Students", "TheEFCkeiForKorsEntityObject", "dbo.Courses");
            //DropIndex("dbo.Rentals", new[] { "PenaltyId" });
            //DropIndex("dbo.Rentals", new[] { "Movie_Id" });
            ////DropIndex("dbo.Students", new[] { "TheEFCkeiForKorsEntityObject" });
            //RenameColumn(table: "dbo.Movies", name: "Id2", newName: "Id");
            //RenameColumn(table: "dbo.Rentals", name: "Customer_Id", newName: "CustomerId");
            //RenameIndex(table: "dbo.Rentals", name: "IX_Customer_Id", newName: "IX_CustomerId");
            //DropColumn("dbo.Rentals", "DateReturned");
            //DropColumn("dbo.Rentals", "PenaltyId");
            //DropColumn("dbo.Rentals", "Movie_Id");
            ////DropTable("dbo.Courses");
            ////DropTable("dbo.Students");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        SomeRandomPropToBeUsedAsPeeKei = c.Int(nullable: false, identity: true),
                        AStringName = c.String(),
                        ABooleanProp = c.Boolean(nullable: false),
                        AnIntegerProp = c.Int(nullable: false),
                        AnotherIntegerProp = c.Int(nullable: false),
                        TheEFCkeiForKorsEntityObject = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SomeRandomPropToBeUsedAsPeeKei);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        SomeRandomPropAsThePeeKei = c.Int(nullable: false, identity: true),
                        AnotherIntegerProp = c.Int(nullable: false),
                        AndAnother = c.Int(nullable: false),
                        TheNameOftheCourse = c.String(),
                    })
                .PrimaryKey(t => t.SomeRandomPropAsThePeeKei);
            
            AddColumn("dbo.Rentals", "Movie_Id", c => c.Int(nullable: false));
            AddColumn("dbo.Rentals", "PenaltyId", c => c.Int());
            AddColumn("dbo.Rentals", "DateReturned", c => c.DateTime());
            RenameIndex(table: "dbo.Rentals", name: "IX_CustomerId", newName: "IX_Customer_Id");
            RenameColumn(table: "dbo.Rentals", name: "CustomerId", newName: "Customer_Id");
            RenameColumn(table: "dbo.Movies", name: "Id", newName: "Id2");
            CreateIndex("dbo.Students", "TheEFCkeiForKorsEntityObject");
            CreateIndex("dbo.Rentals", "Movie_Id");
            CreateIndex("dbo.Rentals", "PenaltyId");
            AddForeignKey("dbo.Students", "TheEFCkeiForKorsEntityObject", "dbo.Courses", "SomeRandomPropAsThePeeKei", cascadeDelete: true);
            AddForeignKey("dbo.Rentals", "PenaltyId", "dbo.Penalties", "Id");
            AddForeignKey("dbo.Rentals", "Movie_Id", "dbo.Movies", "Id2", cascadeDelete: true);
        }
    }
}
