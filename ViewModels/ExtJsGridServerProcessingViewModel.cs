﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.ViewModels
{
    public class ExtJsGridFiltersViewModel
    {
        public ExtJsSortViewModel Sort { get; set; }
        public ExtJsSearchViewModel Search { get; set; }

        public int Take { get; set; }

        public int Skip { get; set; }
    }

    public class ExtJsSortViewModel
    {
        public string property { get; set; }
        public string direction { get; set; }
    }

    public class ExtJsSearchViewModel
    {
        public string @operator { get; set; }
        public string value { get; set; }
        public string property { get; set; }
    }
}