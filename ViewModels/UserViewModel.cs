﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vidly.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }

        //[Required]
        //public string UserName { get; set; }

        [Required]
        public string Email { get; set; }

        public List<string> RoleNames { get; set; }

        [Display(Name = "Roles")]
        public List<string> RoleIds { get; set; }

        public List<string> Roles { get; set; }

        public byte RoleId { get; set; }

        [Required]
        [Display(Name = "Driving License")]
        public string DrivingLicense { get; set; }

        [Required]
        public string Phone { get; set; }




        //public string Id { get; set; }

        ////[Required]
        ////public string UserName { get; set; }

        //[Required]
        //public string Email { get; set; }

        //public List<string> RoleNames { get; set; }

        //[Display(Name = "Roles")]
        //public List<string> RoleIds { get; set; }

        //public List<SelectListItem> Roles { get; set; }

        //public byte RoleId { get; set; }

        //[Required]
        //[Display(Name = "Driving License")]
        //public string DrivingLicense { get; set; }

        //[Required]
        //public string Phone { get; set; }
    }
}