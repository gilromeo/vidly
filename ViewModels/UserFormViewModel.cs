﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.ViewModels
{
    public class UserFormViewModel
    {

        public string Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Display(Name = "Roles")]
        public IList<string> RoleIds { get; set; }

        public List<IdentityRole> Roles { get; set; }

        [Required]
        [Display(Name = "Driving License")]
        [StringLength(30)]
        public string DrivingLicense { get; set; }

        [Required]
        [StringLength(20)]
        public string Phone { get; set; }

    }
}