﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vidly.Models
{
    public class MovieType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public decimal Rate { get; set; }

        [Required]
        public decimal LatePenaltyPerDay { get; set; }

        [Required]
        public byte MaximumRentalPeriodInDays { get; set; }
    }
}