﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Rental
    {
        public int Id { get; set; }

        public DateTime DateRented { get; set; }
        
        // Foreign Keys
        public int PaymentId { get; set; }

        public int CustomerId { get; set; }

        // Navigation Properties
        public virtual ICollection<RentalDetail> RentalDetails { get; set; }

        public virtual Payment Payment { get; set; }

        public virtual Customer Customer { get; set; }
    }
}