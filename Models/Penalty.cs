﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class Penalty
    {
        public int Id { get; set; }

        public decimal Amount { get; set; }

        public int? PaymentId { get; set; }

        public int SomeIntPropForRentalDetailFK { get; set; }

        public Payment Payment { get; set; }

        public RentalDetail RentalDetail { get; set; }
    }
}