﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Vidly.Models.Custom_Validators;

namespace Vidly.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter customer's name")]
        [StringLength(255)]
        [Column("FullName", TypeName ="VARCHAR")]
        public string Name { get; set; }

        [Display(Name = "Date of Birth")]
        [MembershipAgeRequirement]
        public DateTime? Birthday { get; set; }

        public bool IsSubscribedToNewsletter { get; set; }

        public MembershipType MembershipType { get; set; }

        [Display(Name = "Membership Type")]
        public byte MembershipTypeId { get; set; }

        [Range(1, 10)]
        [Display(Name = "Maximum Rental Count")]
        public byte NumberOfAllowableRental { get; set; }

        [Display(Name = "Currently Rented Movies")]
        public byte NumberOfCurrentRental { get; set; }

        public DateTime DateAdded { get; set; }

        public DateTime? DateRegistered { get; set; }

        public bool IsDelinquent { get; set; }
    }
}