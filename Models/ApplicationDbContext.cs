﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Vidly.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Customer> Customers { get; set; }

        public DbSet<Movie> Movies { get; set; }

        public DbSet<MembershipType> MembershipTypes { get; set; }

        public DbSet<Genre> Genres { get; set; }

        public DbSet<MovieType> MovieTypes { get; set; }

        public DbSet<Rental> Rentals { get; set; }

        public DbSet<IdentityUserRole> UserRoles { get; set; }

        public DbSet<MemberSignUp> SignUps { get; set; }

        public DbSet<Penalty> Penalties{ get; set; }

        public DbSet<RentalDetail> RentalDetails { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Vidly.Migrations.Configuration>());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<RentalDetail>().HasKey(rd => rd.IntPropAsPK);
            modelBuilder.Entity<RentalDetail>().HasRequired(rd => rd.Movie).WithMany().HasForeignKey(rd => rd.IntPropAsMovieFK);
            modelBuilder.Entity<RentalDetail>().HasOptional(rd => rd.Penalty).WithRequired();

            modelBuilder.Entity<Penalty>().HasRequired(rd => rd.RentalDetail).WithMany().HasForeignKey(p => p.SomeIntPropForRentalDetailFK);
        }
    }
}