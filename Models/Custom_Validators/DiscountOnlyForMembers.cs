﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Vidly.Dtos;

namespace Vidly.Models.Custom_Validators
{
    public class DiscountOnlyForMembers : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            dynamic customer;
            if (validationContext.ObjectInstance.GetType() == typeof(CustomerDto))
            {
                customer = (CustomerDto)validationContext.ObjectInstance;
            }
            else
            {
                customer = (Customer)validationContext.ObjectInstance;
            }

            if ((customer.MembershipTypeId == MembershipType.Unknown || customer.MembershipTypeId == MembershipType.PayAsYouGo)
                && customer.Discount > 0)
            {
                return new ValidationResult("Customer must be a member to avail discounted rates.");
            }

            return ValidationResult.Success;
        }
    }
}