﻿using System;
using System.ComponentModel.DataAnnotations;
using Vidly.Dtos;

namespace Vidly.Models.Custom_Validators
{
    public class MembershipAgeRequirement : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            dynamic customer;
            if (validationContext.ObjectInstance.GetType() == typeof(CustomerDto))
            {
                customer = (CustomerDto)validationContext.ObjectInstance;
            }
            else
            {
                customer = (Customer)validationContext.ObjectInstance;
            }

            if (customer.MembershipTypeId == MembershipType.Unknown ||
                customer.MembershipTypeId == MembershipType.PayAsYouGo)
            {
                return ValidationResult.Success;
            }

            if (customer.Birthday == null)
            {
                return new ValidationResult("Birthdate is required.");
            }

            var age = DateTime.Today.Year - customer.Birthday.Year;

            return age >= 18
                // Valid
                ? ValidationResult.Success
                // Invalid
                : new ValidationResult("Customer must be 18 years old and above to be a member.");
        }
    }
}