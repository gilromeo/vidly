﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.Models
{
    public class MemberSignUp
    {
        public int Id { get; set; }

        public int? PaymentId { get; set; }

        public Payment Payment { get; set; }

        public int CustomerId { get; set; }
        
        public Customer Customer { get; set; }

        public int MembershipTypeId { get; set; }

        public MembershipType MembershipType { get; set; }
    }
}