﻿using System;

namespace Vidly.Models
{
    public class RentalDetail
    {
        public int IntPropAsPK { get; set; }

        public DateTime? DateReturned { get; set; }


        // Foreign Key
        public int IntPropAsMovieFK { get; set; }

        // Navigation Properties
        public virtual Movie Movie { get; set; }

        public virtual Penalty Penalty { get; set; }
    }
}